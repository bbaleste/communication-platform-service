# Changelog

# [2.0.0]

### Improvements

* refactors
* code coverage
* e-mail and phone validations

# [1.0.0]

### Features

* init version of project
* schedule message
* find scheduled message
* delete scheduled message




