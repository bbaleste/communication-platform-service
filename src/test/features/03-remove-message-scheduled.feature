# language: en

Feature: Remove Message Scheduled

    Background: Message Was Scheduled
        Given ScheduleMessageCommand
        """
        {
          "customerId": "489af5a0-3485-46b0-95c3-cd269e3b603b",
          "content": "Congratulations on your new purchase",
          "platformType": "WHATSAPP",
          "subject": "new purchase",
          "email": "teste@teste.com",
          "phone": "99999999999",
          "dateHourToSend": "2030-07-10T23:15:30-03:00"
        }
        """
        When call create message-scheduler api
        Then should schedule message

    Scenario: Remove message scheduled by messageId
        Given the saved messageId returned in create step
        When remove endpoint was called
        Then should return status 200
        And message was removed

    Scenario: Try remove message scheduled using non exist messageId
        Given messageId not exist in database
        When remove endpoint was called
        Then should return status 404

    Scenario: Try remove message scheduled but message has already sent
        Given the saved messageId returned in create step
        And message has already sent
        When remove endpoint was called
        Then should return status 500
