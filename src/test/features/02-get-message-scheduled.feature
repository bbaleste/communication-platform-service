# language: en

Feature: Find Message Scheduled

    Background: Message Was Scheduled
        Given ScheduleMessageCommand
        """
       {
          "customerId": "489af5a0-3485-46b0-95c3-cd269e3b603b",
          "content": "Congratulations on your new purchase",
          "platformType": "WHATSAPP",
          "subject": "new purchase",
          "email": "teste@teste.com",
          "phone": "99999999999",
          "dateHourToSend": "2030-07-10T23:15:30-03:00"
        }
        """
        When call create message-scheduler api
        Then should schedule message

    Scenario: Get message scheduled find by messageId
        Given the saved messageId returned in create step
        When get endpoint was called
        Then should return status 200
        And should return a valid message

    Scenario: Try get schedule message using messageId that not exist
        Given messageId not exist in database
        When get endpoint was called
        Then should return status 404
