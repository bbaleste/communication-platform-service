# language: en

Feature: Schedule new message

    Scenario: Schedule e-mail message
        Given ScheduleMessageCommand
        """
        {
          "customerId": "489af5a0-3485-46b0-95c3-cd269e3b603b",
          "content": "Congratulations on your new purchase",
          "platformType": "EMAIL",
          "subject": "new purchase",
          "email": "teste@teste.com",
          "dateHourToSend": "2030-07-10T23:15:30-03:00"
        }
        """
        When call create message-scheduler api
        Then should schedule message
        And should return status 201

    Scenario: Try schedule message type e-mail with null email field
        Given ScheduleMessageCommand
        """
        {
          "customerId": "489af5a0-3485-46b0-95c3-cd269e3b603b",
          "content": "Congratulations on your new purchase",
          "platformType": "EMAIL",
          "subject": "new purchase",
          "dateHourToSend": "2030-07-10T23:15:30-03:00"
        }
        """
        When call create message-scheduler api
        Then should return status 422

    Scenario: Try schedule message type e-mail with invalid email format
        Given ScheduleMessageCommand
        """
        {
          "customerId": "489af5a0-3485-46b0-95c3-cd269e3b603b",
          "content": "Congratulations on your new purchase",
          "platformType": "EMAIL",
          "subject": "new purchase",
          "email": "wrong-email",
          "dateHourToSend": "2030-07-10T23:15:30-03:00"
        }
        """
        When call create message-scheduler api
        Then should return status 422

    Scenario: Schedule PUSH message
        Given ScheduleMessageCommand
        """
        {
          "customerId": "489af5a0-3485-46b0-95c3-cd269e3b603b",
          "content": "Congratulations on your new purchase",
          "platformType": "PUSH",
          "subject": "new purchase",
          "phone": "99999999999",
          "dateHourToSend": "2030-07-10T23:15:30-03:00"
        }
        """
        When call create message-scheduler api
        Then should schedule message
        And should return status 201

    Scenario: Schedule SMS message
        Given ScheduleMessageCommand
        """
        {
          "customerId": "489af5a0-3485-46b0-95c3-cd269e3b603b",
          "content": "Congratulations on your new purchase",
          "platformType": "SMS",
          "subject": "new purchase",
          "phone": "99999999999",
          "dateHourToSend": "2030-07-10T23:15:30-03:00"
        }
        """
        When call create message-scheduler api
        Then should schedule message
        And should return status 201

    Scenario: Schedule WHATSAPP message
        Given ScheduleMessageCommand
        """
        {
          "customerId": "489af5a0-3485-46b0-95c3-cd269e3b603b",
          "content": "Congratulations on your new purchase",
          "platformType": "WHATSAPP",
          "subject": "new purchase",
          "phone": "99999999999",
          "dateHourToSend": "2030-07-10T23:15:30-03:00"
        }
        """
        When call create message-scheduler api
        Then should schedule message
        And should return status 201

    Scenario: Try schedule message type sms or whatsapp or push with null phone field
        Given ScheduleMessageCommand
        """
        {
          "customerId": "489af5a0-3485-46b0-95c3-cd269e3b603b",
          "content": "Congratulations on your new purchase",
          "platformType": "PUSH",
          "subject": "new purchase",
          "dateHourToSend": "2030-07-10T23:15:30-03:00"
        }
        """
        When call create message-scheduler api
        Then should return status 422

    Scenario: Try schedule message with null customer
        Given ScheduleMessageCommand
        """
        {
          "content": "Congratulations on your new purchase",
          "platformType": "PUSH",
          "subject": "new purchase",
          "phone": "99999999999",
          "dateHourToSend": "2030-07-10T23:15:30-03:00"
        }
        """
        When call create message-scheduler api
        Then should return status 422

    Scenario: Try schedule message with with null content
        Given ScheduleMessageCommand
        """
        {
          "customerId": "489af5a0-3485-46b0-95c3-cd269e3b603b",
          "platformType": "PUSH",
          "subject": "new purchase",
          "phone": "99999999999",
          "dateHourToSend": "2030-07-10T23:15:30-03:00"
        }
        """
        When call create message-scheduler api
        Then should return status 422

    Scenario: Try schedule message with with null dateHourToSend
        Given ScheduleMessageCommand
        """
        {
          "customerId": "489af5a0-3485-46b0-95c3-cd269e3b603b",
          "content": "Congratulations on your new purchase",
          "platformType": "WHATSAPP",
          "subject": "new purchase",
          "phone": "99999999999"
        }
        """
        When call create message-scheduler api
        Then should return status 422

    Scenario: Try schedule message with with null platformType
        Given ScheduleMessageCommand
        """
        {
          "customerId": "489af5a0-3485-46b0-95c3-cd269e3b603b",
          "content": "Congratulations on your new purchase",
          "subject": "new purchase",
          "phone": "99999999999",
          "dateHourToSend": "2030-07-10T23:15:30-03:00"
        }
        """
        When call create message-scheduler api
        Then should return status 422

    Scenario: Try schedule message with old send date
        Given ScheduleMessageCommand
        """
        {
          "content": "Congratulations on your new purchase",
          "platformType": "PUSH",
          "subject": "new purchase",
          "phone": "99999999999",
          "dateHourToSend": "2010-07-10T23:15:30-03:00"
        }
        """
        When call create message-scheduler api
        Then should return status 422
