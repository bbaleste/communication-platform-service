package br.com.magazineluiza;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.datatable.DataTable;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

@Component
public final class TestUtils {

    private final ObjectMapper objectMapper;

    public TestUtils(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public static <T> Optional<T> getFirst(DataTable dataTable, Type type) {
        List<T> list = dataTable.asList(type);
        return findFirst(list);
    }

    public static <T> Optional<T> findFirst(List<T> list) {
        if (list == null || list.isEmpty()) return empty();

        return ofNullable(list.get(0));
    }

    public static <T> T withField(T target, String fieldName, Object value) {
        Field field = ReflectionUtils.findField(target.getClass(), fieldName);
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, target, value);

        return target;
    }

    public <T> String toJson(DataTable dataTable, Type type) {
        return getFirst(dataTable, type).map(value -> {
            try {
                return objectMapper.writeValueAsString(value);
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }).orElse("");
    }

}
