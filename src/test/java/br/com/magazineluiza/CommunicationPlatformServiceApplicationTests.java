package br.com.magazineluiza;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = CommunicationPlatformServiceApplication.class)
class CommunicationPlatformServiceApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void main() {
        CommunicationPlatformServiceApplication.main(new String[]{});
    }
}
