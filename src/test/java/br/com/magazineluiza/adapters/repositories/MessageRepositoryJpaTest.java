package br.com.magazineluiza.adapters.repositories;

import static br.com.magazineluiza.fixture.MessageFixture.getMessage;
import static org.mockito.BDDMockito.then;

import br.com.magazineluiza.domain.service.MessageRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MessageRepositoryJpaTest {

    @Mock
    private MessageRepositorySpringData repositorySpringData;

    private MessageRepository repository;

    @Before
    public void setUp() {
        repository = new MessageRepositoryJpa(repositorySpringData);
    }

    @Test
    public void givenMessageWhenSaveThenDelegateToJpaRepository() {
        final var message = getMessage();
        repository.save(message);
        then(repositorySpringData).should().save(message);
    }

    @Test
    public void givenMessageIdWhenFindByMessageIdThenDelegateToJpaRepository() {
        final var message = getMessage();
        repository.findByMessageId(message.messageId());
        then(repositorySpringData).should().findById(message.messageId());
    }

    @Test
    public void givenMessageIdWhenRemoveByMessageIdThenDelegateToJpaRepository() {
        final var message = getMessage();
        repository.removeByMessageId(message.messageId());
        then(repositorySpringData).should().deleteById(message.messageId());
    }

}
