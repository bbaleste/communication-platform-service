package br.com.magazineluiza.adapters.web;

import static br.com.magazineluiza.fixture.CreateScheduledMessageFixture.getScheduleMessageCommand;
import static br.com.magazineluiza.fixture.MessageFixture.getMessage;
import static br.com.magazineluiza.fixture.TestConstants.CELL_PHONE;
import static br.com.magazineluiza.fixture.TestConstants.CONTENT;
import static br.com.magazineluiza.fixture.TestConstants.CUSTOMER_ID;
import static br.com.magazineluiza.fixture.TestConstants.EMAIL;
import static br.com.magazineluiza.fixture.TestConstants.MESSAGE_ID;
import static br.com.magazineluiza.fixture.TestConstants.PLATFORM_TYPE;
import static br.com.magazineluiza.fixture.TestConstants.SENT;
import static br.com.magazineluiza.fixture.TestConstants.SUBJECT;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.magazineluiza.application.MessageApplicationService;
import br.com.magazineluiza.application.MessageFactory;
import br.com.magazineluiza.config.JacksonConfiguration;
import br.com.magazineluiza.domain.exception.MessageNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class MessageSchedulerControllerTest {

    private MockMvc mockMvc;

    private ObjectMapper objectMapper;

    @Mock
    private MessageApplicationService messageApplicationService;

    @Before
    public void setUp() {
        initMocks(this);
        JacksonConfiguration jacksonConfiguration = new JacksonConfiguration();
        objectMapper = new ObjectMapper();
        this.mockMvc = MockMvcBuilders.standaloneSetup(new MessageSchedulerController(messageApplicationService))
            .setMessageConverters(jacksonConfiguration.mappingJacksonHttpMessageConverter())
            .build();
    }

    @Test
    public void givenNotExistMessageIdWhenGetShouldReturnNotFound() throws Exception {
        given(messageApplicationService.findByMessageId(any()))
            .willThrow(MessageNotFoundException.class);
        this.mockMvc.perform(
            get("/v1/message-scheduler/" + MESSAGE_ID)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().isNotFound());
    }

    @Test
    public void givenValidMessageIdWhenGetShouldReturnMessage() throws Exception {
        final var messageDto = MessageFactory.toMessageDTO(getMessage());
        given(messageApplicationService.findByMessageId(any()))
            .willReturn(messageDto);
        var verify = this.mockMvc.perform(
            get("/v1/message-scheduler/" + MESSAGE_ID)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().isOk());
        validateMessageResponse(verify);
    }

    @Test
    public void givenValidMessageIdWhenDeleteShouldDeleteMessage() throws Exception {
        this.mockMvc.perform(
            delete("/v1/message-scheduler/" + MESSAGE_ID)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().isOk());
        then(messageApplicationService).should().removeByMessageId(MESSAGE_ID);
    }

    @Test
    public void givenMessageToScheduleThenSaveMessage() throws Exception {
        final var messageDto = MessageFactory.toMessageDTO(getMessage());
        given(messageApplicationService.save(any()))
            .willReturn(messageDto);
        final var content = "{\"customerId\":\"" + CUSTOMER_ID + "\"," +
            "\"subject\":\"" + SUBJECT + "\"," +
            "\"platformType\":\"" + PLATFORM_TYPE + "\"," +
            "\"content\":\"" + CONTENT + "\"," +
            "\"email\":\"" + EMAIL + "\"," +
            "\"phone\":\"" + CELL_PHONE + "\"," +
            "\"dateHourToSend\":\"" + "2022-01-30T23:15:30-03:00" + "\"" +
            "}";
        var request =
            post("/v1/message-scheduler")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        var verify = this.mockMvc.perform(request).andExpect(status().isCreated());
        validateMessageResponse(verify);
    }

    private void validateMessageResponse(ResultActions resultActions) throws Exception {
        resultActions.andExpect(jsonPath("messageId").isNotEmpty())
            .andExpect(jsonPath("customerId").value(CUSTOMER_ID.toString()))
            .andExpect(jsonPath("content").value(CONTENT))
            .andExpect(jsonPath("platformType").value(PLATFORM_TYPE))
            .andExpect(jsonPath("subject").value(SUBJECT))
            .andExpect(jsonPath("email").value(EMAIL))
            .andExpect(jsonPath("phone").value(CELL_PHONE))
            .andExpect(jsonPath("creationDate").isNotEmpty())
            .andExpect(jsonPath("dateHourToSend").isNotEmpty())
            .andExpect(jsonPath("sent").value(SENT));
    }
}
