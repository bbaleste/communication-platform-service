package br.com.magazineluiza.adapters.web.error;

import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;
import static org.springframework.http.ResponseEntity.status;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidatorFactory;

import br.com.magazineluiza.domain.exception.IntegrationException;
import br.com.magazineluiza.domain.exception.IntegrationExceptionType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

public class RestControllerExceptionHandlerTest {

    @Mock
    DataBinder dataBinder;
    @Mock
    IntegrationException integrationException;
    @Mock
    MethodArgumentNotValidException methodArgumentNotValidException;
    @Mock
    ConstraintViolationException constraintViolationException;
    @Mock
    BindingResult bindingResult;
    private RestControllerExceptionHandler restControllerExceptionHandler;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        restControllerExceptionHandler = new RestControllerExceptionHandler();
    }

    @Test
    public void activateDirectFieldAccess() {
        restControllerExceptionHandler.activateDirectFieldAccess(dataBinder);
        Mockito.verify(dataBinder, Mockito.times(1)).initDirectFieldAccess();
    }

    @Test
    public void handleIntegrationException() {
        ResponseEntity<ApiError> expected = status(UNPROCESSABLE_ENTITY).body(
            ApiError.of(IntegrationExceptionType.UNPROCESSABLE_ENTITY, "Error unit test"));

        when(integrationException.type()).thenReturn(IntegrationExceptionType.UNPROCESSABLE_ENTITY);
        when(integrationException.message()).thenReturn("Error unit test");
        when(integrationException.status()).thenReturn(UNPROCESSABLE_ENTITY);

        ResponseEntity<ApiError> result = restControllerExceptionHandler.handleIntegrationException(integrationException);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void handleMethodArgumentNotValidException() {
        ApiError error = ApiError.of(IntegrationExceptionType.NOT_NULL, "Invalid parameters");
        error.setErrors(Collections.singletonList(ValidationError.of("fieldName",
            null, "Default message error", Constraint.of(null))));
        ResponseEntity<ApiError> expected = status(BAD_REQUEST).body(error);

        when(bindingResult.getFieldErrors()).thenReturn(
            Collections.singletonList(new FieldError(
                "objectName",
                "fieldName",
                "Default message error"
            )));
        when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);

        ResponseEntity<ApiError> result = restControllerExceptionHandler
            .handleMethodArgumentNotValidException(methodArgumentNotValidException);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void handleValidationConstraintViolationException() {
        ApiError error = ApiError.of(IntegrationExceptionType.UNPROCESSABLE_ENTITY, "Constraint Violation Exception");
        error.setErrors(Collections.emptyList());
        ResponseEntity<ApiError> expected = status(UNPROCESSABLE_ENTITY).body(error);
        final ValidatorFactory validatorFactory = javax.validation.Validation.buildDefaultValidatorFactory();
        final javax.validation.Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<?>> violations = new HashSet<>();
        when(constraintViolationException.getConstraintViolations())
            .thenReturn(violations);

        ResponseEntity<ApiError> result = restControllerExceptionHandler
            .beanValidationConstraintViolationException(constraintViolationException);

        Assert.assertEquals(expected, result);
    }

}
