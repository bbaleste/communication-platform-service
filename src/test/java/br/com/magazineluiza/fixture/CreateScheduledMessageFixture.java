package br.com.magazineluiza.fixture;

import static br.com.magazineluiza.fixture.TestConstants.CELL_PHONE;
import static br.com.magazineluiza.fixture.TestConstants.CONTENT;
import static br.com.magazineluiza.fixture.TestConstants.CUSTOMER_ID;
import static br.com.magazineluiza.fixture.TestConstants.EMAIL;
import static br.com.magazineluiza.fixture.TestConstants.OFFSETDATETIME;
import static br.com.magazineluiza.fixture.TestConstants.PLATFORM_TYPE;
import static br.com.magazineluiza.fixture.TestConstants.SUBJECT;

import br.com.magazineluiza.application.ScheduleMessageCommand;
import br.com.magazineluiza.shared.enums.PlatformTypeEnum;

public final class CreateScheduledMessageFixture {

    private CreateScheduledMessageFixture() {
    }

    public static ScheduleMessageCommand getScheduleMessageCommand() {
        return ScheduleMessageCommand.builder()
            .phone(CELL_PHONE)
            .content(CONTENT)
            .customerId(CUSTOMER_ID)
            .subject(SUBJECT)
            .dateHourToSend(OFFSETDATETIME)
            .email(EMAIL)
            .platformType(Enum.valueOf(PlatformTypeEnum.class, PLATFORM_TYPE))
            .build();
    }
}
