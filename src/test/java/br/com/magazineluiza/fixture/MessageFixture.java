package br.com.magazineluiza.fixture;

import static br.com.magazineluiza.fixture.TestConstants.CELL_PHONE;
import static br.com.magazineluiza.fixture.TestConstants.CONTENT;
import static br.com.magazineluiza.fixture.TestConstants.CUSTOMER_ID;
import static br.com.magazineluiza.fixture.TestConstants.EMAIL;
import static br.com.magazineluiza.fixture.TestConstants.MESSAGE_ID;
import static br.com.magazineluiza.fixture.TestConstants.OFFSETDATETIME;
import static br.com.magazineluiza.fixture.TestConstants.PLATFORM_TYPE;
import static br.com.magazineluiza.fixture.TestConstants.SENT;
import static br.com.magazineluiza.fixture.TestConstants.SUBJECT;

import br.com.magazineluiza.domain.model.Message;
import br.com.magazineluiza.shared.enums.PlatformTypeEnum;

public final class MessageFixture {

    public MessageFixture() {
    }

    public static Message getMessage() {
        return Message.builder()
            .messageId(MESSAGE_ID)
            .phone(CELL_PHONE)
            .content(CONTENT)
            .subject(SUBJECT)
            .creationDate(OFFSETDATETIME)
            .customerId(CUSTOMER_ID)
            .dateHourToSend(OFFSETDATETIME)
            .email(EMAIL)
            .platformType(Enum.valueOf(PlatformTypeEnum.class, PLATFORM_TYPE))
            .sent(SENT)
            .build();
    }
}
