package br.com.magazineluiza.fixture;

import java.time.OffsetDateTime;
import java.util.Comparator;

public final class TestComparators {

    public static final Comparator<OffsetDateTime> OFFSET_DATE_TIME_COMPARATOR =
        Comparator.comparing(OffsetDateTime::toInstant);

    private TestComparators() {
    }

}
