package br.com.magazineluiza.fixture;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

public final class TestConstants {

    public static final UUID MESSAGE_ID = UUID.fromString("489af5a0-3485-46b0-95c3-cd269e3b603a");
    public static final UUID CUSTOMER_ID = UUID.fromString("489af5a0-3485-46b0-95c3-cd269e3b603b");
    public static final String CELL_PHONE = "99999999999";
    public static final String CONTENT = "Congratulations on your new purchase";
    public static final String SUBJECT = "new purchase";
    public static final String PLATFORM_TYPE = "PUSH";
    public static final String RECEIVER = "Joao da Silva";
    public static final String EMAIL = "teste@teste.com";
    public static final boolean SENT = false;
    public static final OffsetDateTime OFFSETDATETIME = OffsetDateTime.of(2030, 07, 10, 10, 23, 15, 0,
        ZoneOffset.ofHours(-3));

    private TestConstants() {
    }

}
