package br.com.magazineluiza.application;

import static br.com.magazineluiza.fixture.CreateScheduledMessageFixture.getScheduleMessageCommand;
import static br.com.magazineluiza.fixture.MessageFixture.getMessage;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.when;

import br.com.magazineluiza.domain.model.Message;
import br.com.magazineluiza.domain.service.MessageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MessageApplicationServiceTest {

    @InjectMocks
    private MessageApplicationService applicationService;

    @Mock
    private MessageService messageService;


    @Test
    public void givenValidScheduleMessageCommandWhenSaveThenInvokeRepositoryCreate() {
        final var scheduleMessageCommand = getScheduleMessageCommand();
        final var message = getMessage();
        when(messageService.save(any())).thenReturn(message);

        applicationService.save(scheduleMessageCommand);
        then(messageService).should().save(any(Message.class));
    }

    @Test
    public void givenValidMessageIdWhenFindByMessageIdThenInvokeRepository() {
        final var message = getMessage();
        when(messageService.findById(message.messageId())).thenReturn(message);

        applicationService.findByMessageId(message.messageId());
        then(messageService).should()
            .findById(message.messageId());
    }

    @Test
    public void givenValidMessageIdWhenRemoveThenInvokeRepository() {
        final var message = getMessage();

        applicationService.removeByMessageId(message.messageId());
        then(messageService).should()
            .removeByMessageId(message.messageId());
    }
}
