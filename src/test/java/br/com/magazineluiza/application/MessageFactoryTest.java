package br.com.magazineluiza.application;

import static br.com.magazineluiza.application.MessageFactory.toMessage;
import static br.com.magazineluiza.application.MessageFactory.toMessageDTO;
import static br.com.magazineluiza.fixture.CreateScheduledMessageFixture.getScheduleMessageCommand;
import static br.com.magazineluiza.fixture.MessageFixture.getMessage;
import static br.com.magazineluiza.fixture.TestComparators.OFFSET_DATE_TIME_COMPARATOR;
import static org.assertj.core.api.Assertions.assertThat;

import br.com.magazineluiza.domain.model.Message;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MessageFactoryTest {

    @Test
    public void givenScheduleMessageCommandWhenCastToMessageThenGenerateValidObject() {
        final var createScheduledMessage = getScheduleMessageCommand();
        final var message = toMessage(createScheduledMessage);
        compareScheduleMessageCommandAndMessage(createScheduledMessage, message);
    }

    @Test
    public void givenMessageWhenCastToMessageDTOThenGenerateValidObject() {
        final var message = getMessage();
        final var messageDTO = toMessageDTO(message);
        compareMessageAndMessageDTO(messageDTO, message);
    }

    private void compareScheduleMessageCommandAndMessage(ScheduleMessageCommand command,
                                                         Message entity) {
        assertThat(entity)
            .usingRecursiveComparison()
            .withComparatorForFields(OFFSET_DATE_TIME_COMPARATOR, "dateHourToSend")
            .ignoringFields("creationDate", "sent", "messageId")
            .isEqualTo(command);
    }

    private void compareMessageAndMessageDTO(MessageDTO dto, Message entity) {
        assertThat(entity)
            .usingRecursiveComparison()
            .withComparatorForFields(OFFSET_DATE_TIME_COMPARATOR, "dateHourToSend")
            .ignoringFields("creationDate")
            .isEqualTo(dto);
    }
}
