package br.com.magazineluiza.domain.model;

import static br.com.magazineluiza.fixture.TestConstants.CONTENT;
import static br.com.magazineluiza.fixture.TestConstants.CUSTOMER_ID;
import static br.com.magazineluiza.fixture.TestConstants.EMAIL;
import static br.com.magazineluiza.fixture.TestConstants.OFFSETDATETIME;
import static br.com.magazineluiza.fixture.TestConstants.SUBJECT;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import javax.validation.ConstraintViolationException;

import br.com.magazineluiza.domain.exception.MessageCreateException;
import br.com.magazineluiza.shared.enums.PlatformTypeEnum;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MessageTest {

    @Test
    public void shouldThrowExceptionWhenCustomerIdIsNull() {
        assertThatThrownBy(() ->
            Message.scheduleMessage(null, CONTENT, SUBJECT,
                PlatformTypeEnum.EMAIL, EMAIL, null, OFFSETDATETIME))
            .isExactlyInstanceOf(ConstraintViolationException.class)
            .hasMessageStartingWith("customerId: customer of message can't be null");
    }

    @Test
    public void shouldThrowExceptionWhenContentIsNull() {
        assertThatThrownBy(() ->
            Message.scheduleMessage(CUSTOMER_ID, null, SUBJECT,
                PlatformTypeEnum.EMAIL, EMAIL, null, OFFSETDATETIME))
            .isExactlyInstanceOf(ConstraintViolationException.class)
            .hasMessageStartingWith("content: content of message can't be null");
    }

    @Test
    public void shouldThrowExceptionWhenEmailFormatIsWrong() {
        assertThatThrownBy(() ->
            Message.scheduleMessage(CUSTOMER_ID, CONTENT, SUBJECT,
                PlatformTypeEnum.EMAIL, "wrong-email-format", null, OFFSETDATETIME))
            .isExactlyInstanceOf(ConstraintViolationException.class)
            .hasMessageStartingWith("email: wrong e-mail format");
    }

    @Test
    public void shouldThrowExceptionWhenCreationDateIsNotFuture() {
        final var oldDateTime = OffsetDateTime.of(LocalDate.of(2015, 10, 18),
            LocalTime.of(11, 20, 30, 1000),
            ZoneOffset.ofHours(-5));

        assertThatThrownBy(() ->
            Message.scheduleMessage(CUSTOMER_ID, CONTENT, SUBJECT,
                PlatformTypeEnum.EMAIL, EMAIL, null, oldDateTime))
            .isExactlyInstanceOf(ConstraintViolationException.class)
            .hasMessageStartingWith("dateHourToSend: date hour to send needs to be in future");
    }

    @Test
    public void shouldThrowExceptionWhenMessageTypeIsEmailAndEmailFieldIsNull() {
        assertThatThrownBy(() ->
            Message.scheduleMessage(CUSTOMER_ID, CONTENT, SUBJECT,
                PlatformTypeEnum.EMAIL, null, null, OFFSETDATETIME))
            .isExactlyInstanceOf(MessageCreateException.class);
    }

    @Test
    public void shouldThrowExceptionWhenMessageTypeIsSmsAndPhoneFieldIsNull() {
        assertThatThrownBy(() ->
            Message.scheduleMessage(CUSTOMER_ID, CONTENT, SUBJECT,
                PlatformTypeEnum.SMS, EMAIL, null, OFFSETDATETIME))
            .isExactlyInstanceOf(MessageCreateException.class);
    }

    @Test
    public void shouldThrowExceptionWhenMessageTypeIsPushAndPhoneFieldIsNull() {
        assertThatThrownBy(() ->
            Message.scheduleMessage(CUSTOMER_ID, CONTENT, SUBJECT,
                PlatformTypeEnum.PUSH, EMAIL, null, OFFSETDATETIME))
            .isExactlyInstanceOf(MessageCreateException.class);
    }

    @Test
    public void shouldThrowExceptionWhenMessageTypeIsWhatsappAndPhoneFieldIsNull() {
        assertThatThrownBy(() ->
            Message.scheduleMessage(CUSTOMER_ID, CONTENT, SUBJECT,
                PlatformTypeEnum.WHATSAPP, EMAIL, null, OFFSETDATETIME))
            .isExactlyInstanceOf(MessageCreateException.class);
    }

    @Test
    public void shouldCreateMessage() {
        final var message = Message.scheduleMessage(CUSTOMER_ID, CONTENT, SUBJECT,
            PlatformTypeEnum.EMAIL, EMAIL, null, OFFSETDATETIME);
        Assertions.assertNotNull(message.messageId());
        Assertions.assertFalse(message.sent());
        Assertions.assertNull(message.phone());
        Assertions.assertEquals(CUSTOMER_ID, message.customerId());
        Assertions.assertEquals(CONTENT, message.content());
        Assertions.assertEquals(SUBJECT, message.subject());
        Assertions.assertEquals(PlatformTypeEnum.EMAIL, message.platformType());
        Assertions.assertEquals(EMAIL, message.email());
        Assertions.assertEquals(OFFSETDATETIME, message.dateHourToSend());
    }
}
