package br.com.magazineluiza.domain.service;

import static br.com.magazineluiza.application.MessageFactory.toMessage;
import static br.com.magazineluiza.fixture.CreateScheduledMessageFixture.getScheduleMessageCommand;
import static br.com.magazineluiza.fixture.MessageFixture.getMessage;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;

import br.com.magazineluiza.domain.exception.MessageDeleteException;
import br.com.magazineluiza.domain.exception.MessageNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MessageDomainServiceTest {

    @InjectMocks
    private MessageDomainService domainService;

    @Mock
    private MessageRepository repository;

    @Test
    public void givenMessageWhenCallSaveThenValidateInvokeRepository() {
        final var scheduleMessageCommand = getScheduleMessageCommand();
        final var message = toMessage(scheduleMessageCommand);
        domainService.save(message);
        then(repository).should(times(1)).save(message);
    }

    @Test
    public void givenMessageIdWhenCallFindByMessageIdValidateInvokeRepository() {
        final var message = getMessage();
        when(repository.findByMessageId(message.messageId())).thenReturn(Optional.of(message));
        domainService.findById(message.messageId());
        then(repository).should(times(1)).findByMessageId(message.messageId());
    }

    @Test(expected = MessageNotFoundException.class)
    public void givenNotExistMessageIdWhenCallFindByMessageIdValidateException() {
        when(repository.findByMessageId(any())).thenReturn(Optional.empty());
        domainService.findById(UUID.randomUUID());
    }

    @Test
    public void givenMessageIdWhenCallRemoveByMessageIdValidateInvokeRepository() {
        final var message = getMessage();
        when(repository.findByMessageId(message.messageId())).thenReturn(Optional.of(message));
        domainService.removeByMessageId(message.messageId());
        then(repository).should(times(1)).removeByMessageId(message.messageId());
    }

    @Test(expected = MessageNotFoundException.class)
    public void givenNotExistMessageIdWhenCallRemoveByMessageIdValidateException() {
        when(repository.findByMessageId(any())).thenReturn(Optional.empty());
        domainService.removeByMessageId(UUID.randomUUID());
    }

    @Test(expected = MessageDeleteException.class)
    public void givenMessageIdWhenCallRemoveAndMessageAlreadySentThrowException() {
        final var message = getMessage();
        message.sent(true);
        when(repository.findByMessageId(message.messageId())).thenReturn(Optional.of(message));
        domainService.removeByMessageId(message.messageId());
    }

}
