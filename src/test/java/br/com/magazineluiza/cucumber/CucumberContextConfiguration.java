package br.com.magazineluiza.cucumber;

import br.com.magazineluiza.CommunicationPlatformServiceApplication;
import cucumber.api.java.Before;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles("unit-test")
@AutoConfigureTestEntityManager
@ContextConfiguration(classes = CommunicationPlatformServiceApplication.class)
public class CucumberContextConfiguration {

    @Before("setup")
    public void setup_cucumber_spring_context() {
        // Dummy method so cucumber will recognize this class as glue
        // and use its context configuration.
        // https://github.com/cucumber/cucumber-jvm/blob/master/examples/spring-txn/src/test/java/cucumber/examples
        // /spring/txn/CucumberContextConfiguration.java
    }

}
