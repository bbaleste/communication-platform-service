package br.com.magazineluiza.cucumber.stepdefs.config;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.PropertyAccessor.FIELD;
import static java.util.stream.Collectors.toMap;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cucumber.cucumberexpressions.ParameterByTypeTransformer;
import io.cucumber.datatable.TableCellByTypeTransformer;
import io.cucumber.datatable.TableEntryByTypeTransformer;

import java.lang.reflect.Type;
import java.util.Map;

public class GenericTransformer implements ParameterByTypeTransformer, TableEntryByTypeTransformer,
    TableCellByTypeTransformer {

    private final ObjectMapper objectMapper;

    public GenericTransformer() {
        this.objectMapper = new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            .disable(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE)
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .disable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
            .setVisibility(FIELD, ANY);

    }

    @Override
    public Object transform(String s, Type type) {
        return objectMapper.convertValue(s, objectMapper.constructType(type));
    }

    @Override
    public <T> T transform(Map<String, String> map, Class<T> aClass,
                           TableCellByTypeTransformer tableCellByTypeTransformer) {
        Map<String, Object> root = map.entrySet().stream()
            .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));

        Map<String, Map<String, Object>> nodes = map.keySet().stream()
            .filter(key -> key.contains("."))
            .map(key -> key.split("\\.")[0])
            .distinct()
            .collect(toMap(String::toString,
                key -> map.entrySet().stream()
                    .filter(entry -> entry.getKey().startsWith(key))
                    .collect(toMap(o -> o.getKey().split("\\.")[1], Map.Entry::getValue))
            ));

        nodes.forEach(root::put);

        return objectMapper.convertValue(root, aClass);
    }

    @Override
    public <T> T transform(String s, Class<T> aClass) {
        return objectMapper.convertValue(s, aClass);
    }

}
