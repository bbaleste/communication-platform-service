package br.com.magazineluiza.cucumber.stepdefs;

import static br.com.magazineluiza.fixture.TestComparators.OFFSET_DATE_TIME_COMPARATOR;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import javax.annotation.PostConstruct;

import br.com.magazineluiza.TestUtils;
import br.com.magazineluiza.adapters.repositories.MessageRepositorySpringData;
import br.com.magazineluiza.adapters.web.MessageSchedulerController;
import br.com.magazineluiza.adapters.web.error.RestControllerExceptionHandler;
import br.com.magazineluiza.application.MessageDTO;
import br.com.magazineluiza.application.ScheduleMessageCommand;
import br.com.magazineluiza.domain.model.Message;
import cucumber.api.java8.En;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;


public class StepDefs implements En {

    @Autowired
    protected MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private TestUtils testUtils;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MessageSchedulerController messageSchedulerController;

    @Autowired
    private MessageRepositorySpringData repositorySpringData;

    private ScheduleMessageCommand scheduleMessageCommand;
    private ResultActions actions;
    private MockMvc restMockMvc;
    private Message message;
    private UUID messageId;


    public StepDefs() {
        Given("^ScheduleMessageCommand", this::createMessageSchedulerCommand);
        Given("^messageId not exist in database", this::giveAnyMessageId);
        Given("^the saved messageId returned in create step", this::givenSavedMessage);
        When("^call create message-scheduler api$", this::callPostMessage);
        When("^get endpoint was called$", this::callGetMessage);
        When("^remove endpoint was called$", this::callDeleteMessage);
        Then("^should schedule message", this::verifyMessageHasBeenSaved);
        And("^should return status (\\d+)$", this::verifyStatus);
        And("^return should contains field: ([^\"]*)$", this::verifyIfReturnContainsFields);
        And("^([^\"]*) is equal: ([^\"]*)$", this::verifyIfFieldsIsEqualsTo);
        And("^should return a valid message$", this::validateReturnedMessage);
        And("^message was removed$", this::verifyIfMessageWasRemoved);
        And("^message has already sent$", this::setMessageSent);
    }

    @PostConstruct
    private void setup() {

        repositorySpringData.deleteAll();

        restMockMvc = standaloneSetup(messageSchedulerController)
            .setMessageConverters(jacksonMessageConverter)
            .setControllerAdvice(new RestControllerExceptionHandler())
            .build();
    }

    private void createMessageSchedulerCommand(final String command) throws IOException {
        scheduleMessageCommand = objectMapper.readValue(command, ScheduleMessageCommand.class);
    }

    private void callPostMessage() throws Exception {
        var request =
            post("/v1/message-scheduler")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(scheduleMessageCommand));
        actions = restMockMvc.perform(request);
        var contentResult = actions.andReturn().getResponse().getContentAsString();
        message = objectMapper.readValue(contentResult, Message.class);
    }

    private void verifyMessageHasBeenSaved() {
        var optionalMessageScheduler = repositorySpringData.findById(message.messageId());
        assertThat(optionalMessageScheduler).isPresent();
        message = optionalMessageScheduler.get();
        compareMessageSchedulerCommandAndScheduledMessage();
    }

    private void verifyIfMessageWasRemoved() {
        var optionalMessageScheduler = repositorySpringData.findById(message.messageId());
        assertThat(optionalMessageScheduler).isNotPresent();
    }


    private void verifyStatus(final Integer status) throws Exception {
        actions.andExpect(status().is(status));
    }

    private void compareMessageSchedulerCommandAndScheduledMessage() {
        assertThat(message.phone()).isEqualTo(scheduleMessageCommand.phone());
        assertThat(message.content()).isEqualTo(scheduleMessageCommand.content());
        assertThat(message.customerId()).isEqualTo(scheduleMessageCommand.customerId());
        assertThat(message.dateHourToSend().toInstant()).isEqualTo(scheduleMessageCommand.dateHourToSend().toInstant());
        assertThat(message.email()).isEqualTo(scheduleMessageCommand.email());
        assertThat(message.platformType()).isEqualTo(scheduleMessageCommand.platformType());
        assertThat(message.sent()).isFalse();
    }

    private void verifyIfReturnContainsFields(final String fieldsStr) throws Exception {
        var fields = toJsonPathExpressions(fieldsStr);
        for (String field : fields) {
            actions.andExpect(jsonPath(field).exists());
        }
    }

    private List<String> toJsonPathExpressions(final String str) {
        return stream(str.split(","))
            .map(String::trim)
            .map(f -> "$." + f)
            .collect(toList());
    }

    private void verifyIfFieldsIsEqualsTo(final String field, final Object value) throws Exception {
        actions.andExpect(jsonPath(field).value(value));
    }

    private void giveAnyMessageId() {
        messageId = UUID.randomUUID();
    }

    private void givenSavedMessage() {
        messageId = message.messageId();
    }

    private void callGetMessage() throws Exception {
        var request =
            get("/v1/message-scheduler/" + messageId);
        actions = restMockMvc.perform(request);
    }

    private void callDeleteMessage() throws Exception {
        var request =
            delete("/v1/message-scheduler/" + messageId);
        actions = restMockMvc.perform(request);
    }

    private void validateReturnedMessage() throws Exception {
        var jsonReturn = actions.andReturn().getResponse().getContentAsString();
        assertThat(jsonReturn).isNotNull();
        var scheduledMessageReturn = objectMapper.readValue(jsonReturn, MessageDTO.class);
        assertThat(scheduledMessageReturn)
            .usingRecursiveComparison()
            .ignoringFields("creationDate")
            .withComparatorForFields(OFFSET_DATE_TIME_COMPARATOR, "dateHourToSend")
            .isEqualTo(message);
    }

    private void setMessageSent() {
        message.sent(true);
        repositorySpringData.save(message);
    }

}
