package br.com.magazineluiza.cucumber.stepdefs.config;

import java.util.Locale;

import cucumber.api.TypeRegistry;
import cucumber.api.TypeRegistryConfigurer;

public class CustomTypeRegistryConfigurer implements TypeRegistryConfigurer {

    @Override
    public Locale locale() {
        return new Locale("en");
    }

    @Override
    public void configureTypeRegistry(TypeRegistry typeRegistry) {
        GenericTransformer transformer = new GenericTransformer();
        typeRegistry.setDefaultDataTableCellTransformer(transformer);
        typeRegistry.setDefaultDataTableEntryTransformer(transformer);
        typeRegistry.setDefaultParameterTransformer(transformer);
    }

}
