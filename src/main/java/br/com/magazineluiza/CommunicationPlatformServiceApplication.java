package br.com.magazineluiza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommunicationPlatformServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommunicationPlatformServiceApplication.class, args);
    }

}
