package br.com.magazineluiza.domain.service;

import java.util.UUID;

import br.com.magazineluiza.domain.model.Message;

public interface MessageService {

    Message save(final Message message);

    Message findById(final UUID messageId);

    void removeByMessageId(final UUID messageId);
}
