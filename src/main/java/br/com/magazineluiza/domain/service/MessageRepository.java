package br.com.magazineluiza.domain.service;

import java.util.Optional;
import java.util.UUID;

import br.com.magazineluiza.domain.model.Message;

public interface MessageRepository {

    Message save(final Message message);

    Optional<Message> findByMessageId(final UUID messageId);

    void removeByMessageId(final UUID messageId);
}
