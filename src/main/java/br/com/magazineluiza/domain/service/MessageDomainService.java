package br.com.magazineluiza.domain.service;

import java.util.UUID;

import br.com.magazineluiza.domain.exception.MessageDeleteException;
import br.com.magazineluiza.domain.exception.MessageNotFoundException;
import br.com.magazineluiza.domain.model.Message;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MessageDomainService implements MessageService {

    private final MessageRepository messageRepository;

    public MessageDomainService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public Message save(final Message message) {
        return messageRepository.save(message);
    }

    @Override
    public Message findById(final UUID messageId) {
        return messageRepository.findByMessageId(messageId).orElseThrow(MessageNotFoundException::new);
    }

    @Override
    public void removeByMessageId(final UUID messageId) {
        final var scheduledMessage =
            messageRepository.findByMessageId(messageId).orElseThrow(MessageNotFoundException::new);

        if (scheduledMessage.sent())
            throw new MessageDeleteException("Is Not Possible to remove Scheduled Message because was sent");

        messageRepository.removeByMessageId(messageId);
    }
}
