package br.com.magazineluiza.domain.validation;

public interface ValidationHandler {

    default void validate(ValidationHandler validationHandler) {
        DomainValidator.getInstance().validate(validationHandler);
    }

}
