package br.com.magazineluiza.domain.validation;

import java.util.Optional;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;

import br.com.magazineluiza.adapters.validation.CustomMessageInterpolator;

class DomainValidator {

    private static DomainValidator instance;
    private final Validator validator;

    private DomainValidator() {
        this.validator = Validation
            .byDefaultProvider()
            .configure()
            .messageInterpolator(new CustomMessageInterpolator())
            .buildValidatorFactory()
            .getValidator();
    }

    static DomainValidator getInstance() {
        return Optional.ofNullable(instance).orElseGet(DomainValidator::new);
    }

    void validate(ValidationHandler validationHandler) {
        Optional.ofNullable(validationHandler)
            .map(validator::validate)
            .filter(contrains -> !contrains.isEmpty())
            .ifPresent(constraints -> {
                throw new ConstraintViolationException(constraints);
            });
    }
}
