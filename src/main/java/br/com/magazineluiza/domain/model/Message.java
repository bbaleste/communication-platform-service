package br.com.magazineluiza.domain.model;

import static javax.persistence.EnumType.STRING;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import br.com.magazineluiza.domain.exception.MessageCreateException;
import br.com.magazineluiza.domain.validation.ValidationHandler;
import br.com.magazineluiza.shared.enums.PlatformTypeEnum;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
@Entity
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class Message implements Serializable, ValidationHandler {

    @Id
    private UUID messageId;

    @NotNull(message = "customer of message can't be null")
    private UUID customerId;

    @NotNull(message = "content of message can't be null")
    private String content;
    private String subject;

    @NotNull
    @Enumerated(STRING)
    private PlatformTypeEnum platformType;

    @Email(message = "wrong e-mail format")
    private String email;
    private String phone;
    private OffsetDateTime creationDate;

    @NotNull(message = "dateHourToSend can't be null")
    @Future(message = "date hour to send needs to be in future")
    private OffsetDateTime dateHourToSend;
    private boolean sent;

    public Message(UUID messageId,
                   UUID customerId,
                   String content,
                   String subject,
                   PlatformTypeEnum platformType,
                   String email, String phone,
                   OffsetDateTime creationDate,
                   OffsetDateTime dateHourToSend,
                   boolean sent) {
        this.messageId = messageId;
        this.customerId = customerId;
        this.content = content;
        this.subject = subject;
        this.platformType = platformType;
        this.email = email;
        this.phone = phone;
        this.creationDate = creationDate;
        this.dateHourToSend = dateHourToSend;
        this.sent = sent;
        validate(this);
    }

    public static Message scheduleMessage(UUID customerId,
                                          String content,
                                          String subject,
                                          PlatformTypeEnum platformType,
                                          String email,
                                          String phone,
                                          OffsetDateTime dateHourToSend) {
        final var message = new Message(
            UUID.randomUUID(),
            customerId,
            content,
            subject,
            platformType,
            email,
            phone,
            OffsetDateTime.now(),
            dateHourToSend,
            false
        );

        message.validateEmailAndPhoneMessage();

        return message;
    }

    private void validateEmailAndPhoneMessage() {
        if (this.platformType.equals(PlatformTypeEnum.EMAIL) && Optional.ofNullable(email).isEmpty()) {
            throw new MessageCreateException("email field is required for E-mail message");
        }
        if (!this.platformType.equals(PlatformTypeEnum.EMAIL) && Optional.ofNullable(phone).isEmpty()) {
            throw new MessageCreateException("phone field is required for push, whatsapp and sms message");
        }
    }

}
