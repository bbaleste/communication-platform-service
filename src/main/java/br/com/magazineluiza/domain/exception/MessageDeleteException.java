package br.com.magazineluiza.domain.exception;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class MessageDeleteException extends IntegrationException {

    public MessageDeleteException(final String message) {
        super(INTERNAL_SERVER_ERROR, IntegrationExceptionType.ERROR, message);
    }
}
