package br.com.magazineluiza.domain.exception;

import static br.com.magazineluiza.domain.exception.IntegrationExceptionType.NOT_FOUND;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MessageNotFoundException extends IntegrationException {

    public MessageNotFoundException() {
        super(HttpStatus.NOT_FOUND, NOT_FOUND, "Message Not Found");
    }
}
