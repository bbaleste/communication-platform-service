package br.com.magazineluiza.domain.exception;

import static br.com.magazineluiza.domain.exception.IntegrationExceptionType.UNPROCESSABLE_ENTITY;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class MessageCreateException extends IntegrationException {

    public MessageCreateException(final String message) {
        super(HttpStatus.UNPROCESSABLE_ENTITY,
            UNPROCESSABLE_ENTITY, message);
    }
}
