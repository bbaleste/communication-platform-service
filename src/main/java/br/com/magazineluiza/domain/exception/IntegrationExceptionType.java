package br.com.magazineluiza.domain.exception;

public enum IntegrationExceptionType {

    NOT_NULL, NOT_FOUND, ERROR, UNPROCESSABLE_ENTITY
}
