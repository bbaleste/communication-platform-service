package br.com.magazineluiza.adapters.repositories;

import java.util.UUID;

import br.com.magazineluiza.domain.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepositorySpringData extends JpaRepository<Message, UUID> {
}
