package br.com.magazineluiza.adapters.repositories;

import java.util.Optional;
import java.util.UUID;

import br.com.magazineluiza.domain.model.Message;
import br.com.magazineluiza.domain.service.MessageRepository;
import org.springframework.stereotype.Repository;

@Repository
public class MessageRepositoryJpa implements MessageRepository {

    private final MessageRepositorySpringData repositorySpringData;

    public MessageRepositoryJpa(MessageRepositorySpringData repositorySpringData) {
        this.repositorySpringData = repositorySpringData;
    }

    @Override
    public Message save(final Message message) {
        return repositorySpringData.save(message);
    }

    @Override
    public Optional<Message> findByMessageId(final UUID messageId) {
        return repositorySpringData.findById(messageId);
    }

    @Override
    public void removeByMessageId(final UUID messageId) {
        repositorySpringData.deleteById(messageId);
    }
}
