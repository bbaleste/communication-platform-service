package br.com.magazineluiza.adapters.validation;

import java.util.Locale;
import javax.validation.MessageInterpolator;

import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.springframework.context.i18n.LocaleContextHolder;

public class CustomMessageInterpolator implements MessageInterpolator {

    @Override
    public String interpolate(String message, Context context) {
        return new ResourceBundleMessageInterpolator()
            .interpolate(message, context, LocaleContextHolder.getLocale());
    }

    @Override
    public String interpolate(String message, Context context, Locale locale) {
        return new ResourceBundleMessageInterpolator()
            .interpolate(message, context, locale);
    }

}
