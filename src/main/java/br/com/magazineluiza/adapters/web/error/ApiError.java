package br.com.magazineluiza.adapters.web.error;

import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;
import java.util.List;

import br.com.magazineluiza.domain.exception.IntegrationExceptionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor(access = PRIVATE)
@AllArgsConstructor(access = PRIVATE)
@RequiredArgsConstructor(staticName = "of")
public class ApiError implements Serializable {

    @NonNull
    private IntegrationExceptionType type;

    @NonNull
    private String message;
    private List<ValidationError> errors;

}
