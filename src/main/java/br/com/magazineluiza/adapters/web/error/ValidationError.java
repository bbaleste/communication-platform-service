package br.com.magazineluiza.adapters.web.error;

import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = PRIVATE)
@AllArgsConstructor(staticName = "of")
public class ValidationError implements Serializable {

    private String property;
    private Object value;
    private String message;
    private Constraint constraint;

}
