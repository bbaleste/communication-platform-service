package br.com.magazineluiza.adapters.web.error;

import static lombok.AccessLevel.PRIVATE;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = PRIVATE)
@AllArgsConstructor(staticName = "of")
public class Constraint implements Serializable {

    private String name;

}
