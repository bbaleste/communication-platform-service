package br.com.magazineluiza.adapters.web;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

import com.newrelic.api.agent.Trace;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.UUID;
import javax.validation.Valid;

import br.com.magazineluiza.application.MessageApplicationService;
import br.com.magazineluiza.application.MessageDTO;
import br.com.magazineluiza.application.ScheduleMessageCommand;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/message-scheduler")
public class MessageSchedulerController {

    private final MessageApplicationService applicationService;

    public MessageSchedulerController(MessageApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Schedule a new Message")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Message is successfully created and scheduled"),
        @ApiResponse(code = 500, message = "Internal Server Error"),
        @ApiResponse(code = 422, message = "Unprocessable Entity")
    })
    @Trace(dispatcher = true, metricName = "saveMessage")
    public ResponseEntity<MessageDTO> save(@RequestBody ScheduleMessageCommand command) {
        var message = applicationService.save(command);
        return status(CREATED).body(message);
    }

    @GetMapping(path = "/{messageId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Find a Message Scheduled By MessageId")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Message Scheduled is successfully found."),
        @ApiResponse(code = 500, message = "Internal Server Error"),
        @ApiResponse(code = 404, message = "Message Scheduled not Exist in database"),
    })
    @Trace(dispatcher = true, metricName = "getMessage")
    public ResponseEntity<MessageDTO> findByMessageId(@PathVariable UUID messageId) {
        var message = applicationService.findByMessageId(messageId);
        return ok(message);
    }

    @DeleteMapping(path = "/{messageId}")
    @ApiOperation(value = "Remove a message scheduled By MessageId if message not sent yet")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Scheduled Message is successfully removed."),
        @ApiResponse(code = 500, message = "Internal Server Error"),
        @ApiResponse(code = 404, message = "Scheduled Message not Exist in database"),
    })
    @Trace(dispatcher = true, metricName = "deleteMessage")
    public ResponseEntity<String> removeByMessageId(@PathVariable UUID messageId) {
        applicationService.removeByMessageId(messageId);
        return ok().build();
    }
}
