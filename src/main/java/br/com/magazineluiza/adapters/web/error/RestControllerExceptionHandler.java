package br.com.magazineluiza.adapters.web.error;

import static br.com.magazineluiza.domain.exception.IntegrationExceptionType.NOT_NULL;
import static br.com.magazineluiza.domain.exception.IntegrationExceptionType.UNPROCESSABLE_ENTITY;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.unprocessableEntity;

import java.util.List;
import javax.validation.ConstraintViolationException;

import br.com.magazineluiza.domain.exception.IntegrationException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice(annotations = RestController.class)
public class RestControllerExceptionHandler {

    @InitBinder
    public void activateDirectFieldAccess(DataBinder dataBinder) {
        dataBinder.initDirectFieldAccess();
    }

    @ExceptionHandler(IntegrationException.class)
    public ResponseEntity<ApiError> handleIntegrationException(final IntegrationException ex) {
        ApiError error = ApiError.of(ex.type(), ex.message());

        return ResponseEntity.status(ex.status()).body(error);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiError> handleMethodArgumentNotValidException(final MethodArgumentNotValidException ex) {
        ApiError error = ApiError.of(NOT_NULL, "Invalid parameters");

        List<ValidationError> validationErrors =
            ex.getBindingResult().getFieldErrors().stream().map(f -> ValidationError.of(f.getField(),
                f.getRejectedValue(), f.getDefaultMessage(), Constraint.of(f.getCode()))).collect(toList());

        error.setErrors(validationErrors);

        return badRequest().body(error);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ApiError> beanValidationConstraintViolationException(final ConstraintViolationException e) {
        ApiError error = ApiError.of(UNPROCESSABLE_ENTITY, "Constraint Violation Exception");
        List<ValidationError> validationErrors = e.getConstraintViolations()
            .stream()
            .map(ex -> ValidationError.of(
                ex.getPropertyPath().toString(),
                ex.getInvalidValue(),
                ex.getMessage(),
                Constraint.of(ex.getConstraintDescriptor()
                    .getAnnotation()
                    .annotationType()
                    .getSimpleName()))
            )
            .collect(toList());

        error.setErrors(validationErrors);

        return unprocessableEntity().body(error);

    }

}
