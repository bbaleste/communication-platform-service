package br.com.magazineluiza.application;

import java.time.OffsetDateTime;

import br.com.magazineluiza.domain.model.Message;

public final class MessageFactory {

    private MessageFactory() {
    }

    public static Message toMessage(final ScheduleMessageCommand scheduleMessageCommand) {
        return Message.scheduleMessage(
            scheduleMessageCommand.customerId(),
            scheduleMessageCommand.content(),
            scheduleMessageCommand.subject(),
            scheduleMessageCommand.platformType(),
            scheduleMessageCommand.email(),
            scheduleMessageCommand.phone(),
            scheduleMessageCommand.dateHourToSend()
        );
    }

    public static MessageDTO toMessageDTO(final Message message) {
        return MessageDTO.builder()
            .phone(message.phone())
            .content(message.content())
            .creationDate(OffsetDateTime.now())
            .customerId(message.customerId())
            .dateHourToSend(message.dateHourToSend())
            .subject(message.subject())
            .email(message.email())
            .messageId(message.messageId())
            .platformType(message.platformType())
            .sent(message.sent())
            .build();
    }
}
