package br.com.magazineluiza.application;

import static br.com.magazineluiza.application.MessageFactory.toMessage;
import static br.com.magazineluiza.application.MessageFactory.toMessageDTO;

import java.util.UUID;

import br.com.magazineluiza.domain.service.MessageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MessageApplicationService {

    private final MessageService messageService;

    public MessageApplicationService(MessageService messageService) {
        this.messageService = messageService;
    }

    public MessageDTO save(final ScheduleMessageCommand scheduleMessageCommand) {
        final var command = toMessage(scheduleMessageCommand);
        final var message = messageService.save(command);
        return toMessageDTO(message);
    }

    public MessageDTO findByMessageId(final UUID messageId) {
        final var message = messageService.findById(messageId);
        return toMessageDTO(message);
    }


    public void removeByMessageId(final UUID messageId) {
        messageService.removeByMessageId(messageId);
    }
}
