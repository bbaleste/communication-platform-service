package br.com.magazineluiza.application;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.UUID;

import br.com.magazineluiza.shared.enums.PlatformTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@Accessors(fluent = true)
@AllArgsConstructor
@NoArgsConstructor
public class MessageDTO implements Serializable {

    private UUID messageId;
    private UUID customerId;
    private String content;
    private PlatformTypeEnum platformType;
    private String subject;
    private String email;
    private String phone;
    private OffsetDateTime creationDate;
    private OffsetDateTime dateHourToSend;
    private boolean sent;
}
