package br.com.magazineluiza.application;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import br.com.magazineluiza.shared.enums.PlatformTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@Builder
@Accessors(fluent = true)
@AllArgsConstructor
@NoArgsConstructor
public class ScheduleMessageCommand implements Serializable {

    private UUID customerId;
    private String subject;
    private PlatformTypeEnum platformType;
    private String content;
    private String email;
    private String phone;

    @Valid
    @DateTimeFormat(iso = DATE_TIME)
    private OffsetDateTime dateHourToSend;
}
