package br.com.magazineluiza.shared.enums;

public enum PlatformTypeEnum {
    EMAIL, SMS, PUSH, WHATSAPP;
}
