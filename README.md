# ![LuizaLabs](luizalabs.png) communication-platform-service

## What is this project?

It's a project that provides a platform of communication **operations**

* Create Schedule Message
* Find Schedule Message By MessageId
* Remove Schedule Message If Not send yet

## How to use it?

* This project use this base path: *communication-platform-service/*

## Endpoints

All endpoints of API are described in swagger page: /communication-platform-service/swagger-ui.html#/

### Frameworks and technologies

* Java 11
* Spring Boot (Web, Data JPA)
* Swagger
* PostgreSql
* Mockk
* Gradle
* Cucumber

## Build & Run

Prerequisites:

* Java 11
* Docker
* Docker Compose

### Build

This project uses Gradle Wrapper, so to build:

```bash
./gradlew composeUp
./gradlew clean build
```

During the build lifecycle some tasks are executed (compile, test...). Before tests,
a [Gradle plugin](https://github.com/avast/gradle-docker-compose-plugin) runs [docker-compose](docker-compose.yml) to
prepare the dependencies to be used on integration tests:

* PostgreSql

### Run

After build, the artifact `app.jar` is generated and it's ready to run. Before, you must run docker-compose with
dependencies:

```bash
./gradlew composeUp
```

And to run the application locally:

```bash
java -jar -Dspring.profiles.active=local build/libs/app.jar
```

NOTE: the docker-compose must always be run through `composeUp` Gradle task (and not directly `docker-compose up`)

## Documentation

* Further API documentation can be found on `{host}:{port}/swagger-ui.html`

## Maintainers

* Bruna Brito Baleste (brunabrito.blb@gmail.com)
